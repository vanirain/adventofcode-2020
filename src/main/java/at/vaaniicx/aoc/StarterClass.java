package at.vaaniicx.aoc;

import at.vaaniicx.aoc.day.*;

import java.io.IOException;

public class StarterClass {

    public static void main(String[] args) throws IOException {

        Day10.solve();
    }
}
