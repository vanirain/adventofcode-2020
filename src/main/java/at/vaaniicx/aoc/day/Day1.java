package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Day1 {

    private Day1() {

    }

    public static void solve() throws IOException {
        List<Integer> numbers = FileHelper.getLinesFromFile(FileHelper.getFileName(1)).stream().map(Integer::parseInt).collect(Collectors.toList());

        System.out.println("The sum of the two numbers (x + y = 2020) is: " + solvePart1(numbers));
        System.out.println("The sum of the three numbers (x + y + z = 2020) is: " + solvePart2(numbers));
    }

    /**
     * --- Day 1: Report Repair ---
     *
     * After saving Christmas five years in a row, you've decided to take a vacation at a nice resort on a
     * tropical island. Surely, Christmas will go on without you.
     *
     * The tropical island has its own currency and is entirely cash-only. The gold coins used there have a
     * little picture of a starfish; the locals just call them stars. None of the currency exchanges seem to
     * have heard of them, but somehow, you'll need to find fifty of these coins by the time you arrive so
     * you can pay the deposit on your room.
     *
     * To save your vacation, you need to get all fifty stars by December 25th.
     *
     * Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar;
     * the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!
     *
     * Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input);
     * apparently, something isn't quite adding up.
     *
     * Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.
     *
     * Find the two entries that sum to 2020;
     * what do you get if you multiply them together?
     *
     * @param numbers   the puzzle input
     * @return          the puzzle answer
     * @author          vanirain
     * @since           2020
     */
    private static int solvePart1(List<Integer> numbers) {
        for (Integer n : numbers) {
            for (Integer n2 : numbers) {
                if (n + n2 == 2020) {
                    return n * n2;
                }
            }
        }

        return -1;
    }

    /**
     * --- Part Two ---
     *
     * The Elves in accounting are thankful for your help; one of them even offers you a starfish coin they had left
     * over from a past vacation. They offer you a second one if you can find three numbers in your expense report that
     * meet the same criteria.
     *
     * In your expense report, what is the product of the three entries that sum to 2020?
     *
     * @param numbers   the puzzle input
     * @return          the puzzle answer
     * @author          vanirain
     * @since           2020
     */
    private static int solvePart2(List<Integer> numbers) {
        for (Integer n : numbers) {
            for (Integer n2 : numbers) {
                for (Integer n3 : numbers) {
                    if (n + n2 + n3 == 2020) {
                        return n * n2 * n3;
                    }
                }
            }
        }

        return -1;
    }
}
