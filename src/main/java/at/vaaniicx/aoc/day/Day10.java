package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day10 {

    private Day10() {

    }

    public static void solve() throws IOException {
        List<Integer> numbers = FileHelper.getLinesFromFile(FileHelper.getFileName(10)).stream().map(Integer::new).collect(Collectors.toList());
        Collections.sort(numbers);

        System.out.println("The number of 1-jolt differences multiplied by the number fo 3-jolt differences is: " + solvePart1(numbers));
        System.out.println("// : " + solvePart2(numbers));
    }

    /**
     * --- Day 10: Adapter Array ---
     *
     * Each of your joltage adapters is rated for a specific output joltage (your puzzle input). Any given adapter
     * can take an input 1, 2, or 3 jolts lower than its rating and still produce its rated output joltage.
     *
     * In addition, your device has a built-in joltage adapter rated for 3 jolts higher than the highest-rated adapter
     * in your bag. (If your adapter list were 3, 9, and 6, your device's built-in adapter would be rated for 12 jolts.)
     *
     * Treat the charging outlet near your seat as having an effective joltage rating of 0.
     *
     * If you use every adapter in your bag at once, what is the distribution of joltage differences between the
     * charging outlet, the adapters, and your device?
     *
     * What is the number of 1-jolt differences multiplied by the number of 3-jolt differences?
     *
     * @param numbers the puzzle input (sorted)
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart1(List<Integer> numbers) {
        Map<Integer, Integer> joltDifferences = initializeJoltageDifferencesMap();

        for (int i = 0; i < numbers.size(); i++) {
            int prevJoltage = 0;
            int joltage = numbers.get(i);

            if (i != 0) {
                prevJoltage = numbers.get(i - 1);
            }

            int difference = Math.abs(joltage - prevJoltage);
            joltDifferences.put(difference, joltDifferences.get(difference) + 1);

            if (i == numbers.size() - 1) {
                joltDifferences.put(3, joltDifferences.get(3) + 1);

                return joltDifferences.get(1) * joltDifferences.get(3);
            }
        }

        return -1;
    }

    private static Map<Integer, Integer> initializeJoltageDifferencesMap() {
        Map<Integer, Integer> joltDifferences = new HashMap<>();
        for (int i = 1; i <= 3; i++) {
            joltDifferences.put(i, 0);
        }

        return joltDifferences;
    }

    /**
     * --- Part Two ---
     *
     * To completely determine whether you have enough adapters, you'll need to figure out how many different ways
     * they can be arranged. Every arrangement needs to connect the charging outlet to your device. The previous rules
     * about when adapters can successfully connect still apply.
     *
     * You glance back down at your bag and try to remember why you brought so many adapters; there must be more than
     * a trillion valid ways to arrange them! Surely, there must be an efficient way to count the arrangements.
     *
     * What is the total number of distinct ways you can arrange the adapters to connect the charging outlet to
     * your device?
     *
     * @param numbers the puzzle input (sorted)
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart2(List<Integer> numbers) {
        return -1;
    }
}
