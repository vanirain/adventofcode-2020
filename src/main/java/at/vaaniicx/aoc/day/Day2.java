package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.List;

public class Day2 {

    private Day2() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(2));

        System.out.println("There are " + solvePart1(lines) + " passwords valid!");
        System.out.println("There are " + solvePart2(lines) + " passwords valid!");
    }

    /**
     * --- Day 2: Password Philosophy ---
     * Your flight departs in a few days from the coastal airport;
     * the easiest way down to the coast from here is via toboggan.
     *
     * The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day.
     * "Something's wrong with our computers; we can't log in!" You ask if you can take a look.
     *
     * Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by
     * the Official Toboggan Corporate Policy that was in effect when they were chosen.
     *
     * To try to debug the problem, they have created a list (your puzzle input) of passwords
     * (according to the corrupted database) and the corporate policy when that password was set.
     *
     * Each line gives the password policy and then the password. The password policy indicates the lowest and highest
     * number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the
     * password must contain a at least 1 time and at most 3 times.
     *
     * How many passwords are valid according to their policies?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2020
     */
    private static int solvePart1(List<String> lines) {
        int valids = 0;

        String[] splits, minmax;
        int min, max, occurances;
        char letter;
        String password;
        for (String line : lines) {
            splits = line.split(" ");
            minmax = splits[0].split("-");
            min = Integer.parseInt(minmax[0]);
            max = Integer.parseInt(minmax[1]);
            letter = splits[1].charAt(0);
            password = splits[2];
            occurances = 0;

            for (char c : password.toCharArray()) {
                if (c == letter) occurances++;
            }

            if (occurances <= max && occurances >= min) valids++;
        }

        return valids;
    }

    /**
     * --- Part Two ---
     *
     * While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan
     * Corporate Authentication System is expecting.
     *
     * The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old
     * job at the sled rental place down the street! The Official Toboggan Corporate Policy actually works a little
     * differently.
     *
     * Each policy actually describes two positions in the password, where 1 means the first character, 2 means the
     * second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!)
     * Exactly one of these positions must contain the given letter. Other occurrences of the letter are irrelevant
     * for the purposes of policy enforcement.
     *
     * How many passwords are valid according to the new interpretation of the policies?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2020
     */
    private static int solvePart2(List<String> lines) {
        int valids = 0;

        String[] splits, minmax;
        int min, max;
        char letter;
        String password;
        for (String line : lines) {
            splits = line.split(" ");
            minmax = splits[0].split("-");
            min = Integer.parseInt(minmax[0]);
            max = Integer.parseInt(minmax[1]);
            letter = splits[1].charAt(0);
            password = splits[2];

            if (password.charAt(min - 1) == letter ^ password.charAt(max - 1) == letter) valids++;
        }

        return valids;
    }
}
