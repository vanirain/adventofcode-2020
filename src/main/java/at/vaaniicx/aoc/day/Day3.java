package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.List;

public class Day3 {

    private Day3() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(3));

        System.out.println("Number of encountered trees: " + solvePart1(lines));
        System.out.println("Number of encountered trees: " + solvePart2(lines));
    }

    /**
     * --- Day 3: Toboggan Trajectory ---
     * With the toboggan login problems resolved, you set off toward the airport. While travel by toboggan might be
     * easy, it's certainly not safe: there's very minimal steering and the area is covered in trees. You'll need to
     * see which angles will take you near the fewest trees.
     *
     * Due to the local geology, trees in this area only grow on exact integer coordinates in a grid. You make a
     * map (your puzzle input) of the open squares (.) and trees (#) you can see.
     *
     * You start on the open square (.) in the top-left corner and need to reach the bottom
     * (below the bottom-most row on your map).
     *
     * The toboggan can only follow a few specific slopes (you opted for a cheaper model that prefers rational numbers);
     * start by counting all the trees you would encounter for the slope right 3, down 1:
     *
     * From your starting position at the top-left, check the position that is right 3 and down 1. Then, check the
     * position that is right 3 and down 1 from there, and so on until you go past the bottom of the map.
     *
     * Starting at the top-left corner of your map and following a slope of right 3 and down 1,
     * how many trees would you encounter?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2020
     */
    private static int solvePart1(List<String> lines) {
        char[][] array = new char[lines.size()][lines.get(0).length()];
        fillArray(lines, array);

        int counter = 0, trees = 0;
        char c;
        for (char[] chars : array) {
            c = chars[counter % chars.length];
            if (c == '#') {
                trees++;
            }

            counter += 3;
        } // line + 1 = 1 runta

        return trees;
    }

    /**
     * --- Part Two ---
     *
     * Time to check the rest of the slopes -
     * you need to minimize the probability of a sudden arboreal stop, after all.
     *
     * Determine the number of trees you would encounter if, for each of the following slopes, you start at the
     * top-left corner and traverse the map all the way to the bottom:
     *
     *  - Right 1, down 1.
     *  - Right 3, down 1. (This is the slope you already checked.)
     *  - Right 5, down 1.
     *  - Right 7, down 1.
     *  - Right 1, down 2.
     *
     * What do you get if you multiply together the number of trees encountered on each of the listed slopes?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2020
     */
    private static long solvePart2(List<String> lines) {
        char[][] array = new char[lines.size()][lines.get(0).length()];
        fillArray(lines, array);

        long trees = 1;
        trees *= countEncounteredTrees(array, 1, 1);
        trees *= countEncounteredTrees(array, 3, 1);
        trees *= countEncounteredTrees(array, 5, 1);
        trees *= countEncounteredTrees(array, 7, 1);
        trees *= countEncounteredTrees(array, 1, 2);

        return trees;
    }

    private static int countEncounteredTrees(char[][] array, int right, int down) {
        int counter = 0;
        int trees = 0;
        char c;
        for (int i = 0; i < array.length; i += down) {
            c = array[i][counter % array[i].length];
            if (c == '#') {
                trees++;
            }

            counter += right;
        }

        return trees;
    }

    private static void fillArray(List<String> lines, char[][] array) {
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines.get(i).toCharArray().length; j++) {
                array[i][j] = lines.get(i).charAt(j);
            }
        }
    }
}
