package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.object.Passport;
import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day4 {

    private Day4() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(4));

        System.out.println("Number of valid passports found: " + solvePart1(lines));
        System.out.println("Number of valid passports found: " + solvePart2(lines));
    }

    /**
     * --- Day 4: Passport Processing ---
     *
     * You arrive at the airport only to realize that you grabbed your North Pole Credentials instead of your passport.
     * While these documents are extremely similar, North Pole Credentials aren't issued by a country and therefore
     * aren't actually valid documentation for travel in most of the world.
     *
     * It seems like you're not the only one having problems, though; a very long line has formed for the automatic
     * passport scanners, and the delay could upset your travel itinerary.
     *
     * Due to some questionable network security, you realize you might be able to solve both of these problems
     * at the same time.
     *
     * The automatic passport scanners are slow because they're having trouble detecting which passports have all required fields. The expected fields are as follows:
     *
     *  - byr (Birth Year)
     *  - iyr (Issue Year)
     *  - eyr (Expiration Year)
     *  - hgt (Height)
     *  - hcl (Hair Color)
     *  - ecl (Eye Color)
     *  - pid (Passport ID)
     *  - cid (Country ID)
     *
     * Passport data is validated in batch files (your puzzle input). Each passport is represented as a sequence of
     * key:value pairs separated by spaces or newlines. Passports are separated by blank lines.
     *
     * Count the number of valid passports - those that have all required fields.
     * Treat cid as optional. In your batch file, how many passports are valid?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart1(List<String> lines) {
        int valids = 0;
        Passport passport;
        List<String[]> data = new ArrayList<>();
        for (String line : lines) {
            if (!line.isEmpty()) {
                for (String field : line.split(" ")) {
                    data.add(field.split(":"));
                }

                continue;
            }

            passport = new Passport(new ArrayList<>(data));
            if (passport.hasRequiredFields()) {
                valids++;
            }

            data.clear();
        }

        return valids;
    }

    /**
     * --- Part Two ---
     *
     * The line is moving more quickly now, but you overhear airport security talking about how passports with invalid
     * data are getting through. Better add some data validation, quick!
     *
     * You can continue to ignore the cid field, but each other field has strict rules about what values are valid for
     * automatic validation:
     *
     *  - byr (Birth Year)          - four digits; at least 1920 and at most 2002.
     *  - iyr (Issue Year)          - four digits; at least 2010 and at most 2020.
     *  - eyr (Expiration Year)     - four digits; at least 2020 and at most 2030.
     *  - hgt (Height)              - a number followed by either cm or in:
     *      - If cm, the number must be at least 150 and at most 193.
     *      - If in, the number must be at least 59 and at most 76.
     *  - hcl (Hair Color)          - a # followed by exactly six characters 0-9 or a-f.
     *  - ecl (Eye Color)           - exactly one of: amb blu brn gry grn hzl oth.
     *  - pid (Passport ID)         - a nine-digit number, including leading zeroes.
     *  - cid (Country ID)          - ignored, missing or not.
     *
     * Count the number of valid passports - those that have all required fields and valid values.
     * Continue to treat cid as optional. In your batch file, how many passports are valid?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart2(List<String> lines) {
        List<Passport> passports = new ArrayList<>();
        List<String[]> passportData = new ArrayList<>();
        for (String line : lines) {
            if (!line.isEmpty()) {
                for (String field : line.split(" ")) {
                    passportData.add(field.split(":"));
                }

                continue;
            }

            passports.add(new Passport(passportData));
            passportData.clear();
        }

        int valids = 0;
        for (Passport passport : passports) {
            if (!passport.hasRequiredFields()) {
                continue;
            }

            boolean isValid = passport.validateFields();
            if (isValid) {
                valids++;
            }
        }

        return valids;
    }
}
