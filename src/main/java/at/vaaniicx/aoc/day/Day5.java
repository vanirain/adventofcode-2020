package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.object.Seat;
import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Day5 {

    private Day5() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(5));

        System.out.println("Highest seat ID: " + solvePart1(lines));
        System.out.println("My seat ID is: " + solvePart2(lines));
    }

    /**
     * --- Day 5: Binary Boarding ---
     *
     * You board your plane only to discover a new problem: you dropped your boarding pass! You aren't sure which seat
     * is yours, and all of the flight attendants are busy with the flood of people that suddenly made it through
     * passport control.
     *
     * You write a quick program to use your phone's camera to scan all of the nearby boarding passes
     * (your puzzle input); perhaps you can find your seat through process of elimination.
     *
     * Instead of zones or groups, this airline uses binary space partitioning to seat people. A seat might be
     * specified like "FBFBBFFRLR", where "F" means "front", "B" means "back", "L" means "left", and "R" means "right".
     *
     * The first 7 characters will either be "F" or "B"; these specify exactly one of the 128 rows on the plane
     * (numbered 0 through 127). Each letter tells you which half of a region the given seat is in. Start with the
     * whole list of rows; the first letter indicates whether the seat is in the front (0 through 63) or the back
     * (64 through 127). The next letter indicates which half of that region the seat is in, and so on until you're
     * left with exactly one row.
     *
     * The last three characters will be either "L" or "R"; these specify exactly one of the 8 columns of seats on the
     * plane (numbered 0 through 7). The same process as above proceeds again, this time with only three steps.
     * "L" means to keep the lower half, while "R" means to keep the upper half.
     *
     * So, decoding "FBFBBFFRLR" reveals that it is the seat at row 44, column 5.
     *
     * Every seat also has a unique seat ID: multiply the row by 8, then add the column.
     *
     * As a sanity check, look through your list of boarding passes. What is the highest seat ID on a boarding pass?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart1(List<String> lines) {
        List<Seat> seats = lines.stream().map(Seat::new).collect(Collectors.toList());
        List<Integer> ids = seats.stream().map(Seat::getSeatID).collect(Collectors.toList());

        return Collections.max(ids);
    }

    /**
     * --- Part Two ---
     *
     * Ding! The "fasten seat belt" signs have turned on. Time to find your seat.
     *
     * It's a completely full flight, so your seat should be the only missing boarding pass in your list. However,
     * there's a catch: some of the seats at the very front and back of the plane don't exist on this aircraft, so
     * they'll be missing from your list as well.
     *
     * Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will be in your list.
     *
     * What is the ID of your seat?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart2(List<String> lines) {
        List<Seat> seats = lines.stream().map(Seat::new).collect(Collectors.toList());
        List<Integer> ids = seats.stream().map(Seat::getSeatID).sorted().collect(Collectors.toList());

        int num = ids.get(0) - 1;
        for (Integer id : ids) {
            if (num != id - 1) {
                return id - 1;
            }

            num = id;
        }

        return num;
    }
}
