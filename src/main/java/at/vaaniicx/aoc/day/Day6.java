package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.object.Group;
import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day6 {

    private Day6() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(6));

        System.out.println("The sum of the number of questions to which anyone answered \"yes\" is: " + solvePart1(lines));
        System.out.println("The sum of the number of questions to which everyone answered \"yes\" is: " + solvePart2(lines));
    }

    /**
     * --- Day 6: Custom Customs ---
     *
     * As your flight approaches the regional airport where you'll switch to a much larger plane, customs declaration
     * forms are distributed to the passengers.
     *
     * The form asks a series of 26 yes-or-no questions marked a through z. All you need to do is identify the questions
     * for which anyone in your group answers "yes". Since your group is just you, this doesn't take very long.
     *
     * However, the person sitting next to you seems to be experiencing a language barrier and asks if you can help.
     * For each of the people in their group, you write down the questions for which they answer "yes", one per line.
     *
     * Another group asks for your help, then another, and eventually you've collected answers from every group on the
     * plane (your puzzle input). Each group's answers are separated by a blank line, and within each group, each
     * person's answers are on a single line.
     *
     * For each group, count the number of questions to which anyone answered "yes".
     * What is the sum of those counts?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart1(List<String> lines) {
        List<Group> groups = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (String line : lines) {
            if (!line.isEmpty()) {
                stringBuilder.append(line);

                continue;
            }

            groups.add(new Group(stringBuilder.toString().toCharArray()));
            stringBuilder = new StringBuilder();
        }

        int sum = 0;
        for (Group group : groups) {
            sum += group.getUniqueAnswers().size();
        }

        return sum;
    }

    /**
     * --- Part Two ---
     *
     * As you finish the last group's customs declaration, you notice that you misread one word in the instructions:
     *
     * You don't need to identify the questions to which anyone answered "yes"; you need to identify the questions to
     * which everyone answered "yes"!
     *
     * For each group, count the number of questions to which everyone answered "yes".
     * What is the sum of those counts?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart2(List<String> lines) {
        List<Group> groups = new ArrayList<>();
        List<String> data = new ArrayList<>();
        for (String line : lines) {
            if (!line.isEmpty()) {
                data.add(line);
                continue;
            }

            groups.add(new Group(data));
            data.clear();
        }

        int sum = 0;
        for (Group group : groups) {
            List<String> answersPerMember = group.getAnswersPerMember();
            for (char c : answersPerMember.get(0).toCharArray()) {
                int index;
                boolean allAnswered = true;
                for (int i = 1; i < answersPerMember.size(); i++) {
                    index = answersPerMember.get(i).indexOf(c);

                    if (index == -1) {
                        allAnswered = false;
                        break;
                    }
                }

                if (allAnswered) {
                    sum++;
                }
            }
        }

        return sum;
    }
}
