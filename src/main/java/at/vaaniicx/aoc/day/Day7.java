package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.object.Bag;
import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Day7 {

    private Day7() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(7));

        System.out.println("//" + solvePart1(lines));
        System.out.println("//" + solvePart2(lines));
    }

    /**
     * --- Day 7: Handy Haversacks ---
     *
     * You land at the regional airport in time for your next flight. In fact, it looks like you'll even have time to
     * grab some food: all flights are currently delayed due to issues in luggage processing.
     *
     * Due to recent aviation regulations, many rules (your puzzle input) are being enforced about bags and their
     * contents; bags must be color-coded and must contain specific quantities of other color-coded bags. Apparently,
     * nobody responsible for these regulations considered how long they would take to enforce!
     *
     * How many bag colors can eventually contain at least one shiny gold bag?
     * (The list of rules is quite long; make sure you get all of it.)
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart1(List<String> lines) {
//        List<String> filteredShinyGoldBags = getFilteredBags(lines, "shiny gold");
//
//        List<String> bagNames = getBagNames(filteredShinyGoldBags);
//        bagNames = bagNames.stream().map(s -> s.substring(0, s.length() - 1)).collect(Collectors.toList());
//        bagNames.removeIf(s -> s.contentEquals("shiny gold"));
//        bagNames.forEach(System.out::println);
//
//        int eventuallyContains = 0;
//        for (String line : lines) {
//            for (String bagName : bagNames) {
//                if (line.contains(bagName)) {
//                    eventuallyContains++;
//                    break;
//                }
//            }
//        }
//
//        eventuallyContains += bagNames.size() - 1;
//
//        return eventuallyContains;

        List<Bag> allBags = retrieveAllBagsFromInput(lines);
        List<Bag> rules = getRules(allBags);
        int canContain = rules.size();
        int totalBags = allBags.size();
        for (int i = 0; i < 10; i++) {
            List<Bag> newRules = new ArrayList<>();
            for (Bag bag : allBags) {
                if (bag.containsSubBag(rules)) {
                    newRules.add(bag);
                    canContain++;
                }
            }

            allBags.removeAll(newRules);
            rules = newRules;
            System.out.println("iteration " + i + " - bagSize: " + allBags.size());
        }

        System.out.println("totalBags: " + totalBags);
        System.out.println("canContain " + (canContain));






//        List<Bag> rules = getRules(allBags);
//        List<Bag> canContainBag = canContainBag(allBags, rules);


        return -1;
    }

    /**
     * --- Part Two ---
     *
     * [???]
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart2(List<String> lines) {
        return -1;
    }

    private static List<Bag> retrieveAllBagsFromInput(List<String> lines) {
        List<Bag> bags = new ArrayList<>();
        Map<String, Integer> subBags;
        for (String line : lines) {
            subBags = new HashMap<>();

            String[] splits = line.split("contain");
            String bagName = splits[0].split("bag")[0].trim();

            if (line.contains("no other bags")) {
                bags.add(new Bag(bagName, subBags));
                continue;
            }

            String subBagsString = splits[1];
            if (subBagsString.contains(",")) {
                String[] subSplits = subBagsString.split(", ");

                for (String subSplit : subSplits) {
                    getSubBagFromSplit(subBags, subBagsString, subSplit);
                }
            } else {
                getSubBagFromSplit(subBags, subBagsString, subBagsString);
            }

            bags.add(new Bag(bagName, subBags));
        }

        return bags;
    }

    private static void getSubBagFromSplit(Map<String, Integer> subBags, String subBagsString, String subSplit) {
        int amount = Integer.parseInt(subBagsString.trim().substring(0, 1));
        String subBagName = subSplit.substring(2).split("bag")[0].trim();
        subBags.put(subBagName, amount);
    }

    private static List<Bag> getRules(List<Bag> bags) {
        return bags.stream().filter(bag -> bag.containsSubBag("shiny gold")).collect(Collectors.toList());
    }

    private static List<Bag> canContainBag(List<Bag> bags, List<Bag> rules) {
        return bags.stream().filter(bag -> bag.containsSubBag(rules)).collect(Collectors.toList());
    }
}
