package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.object.Operation;
import at.vaaniicx.aoc.object.type.OperationType;
import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day8 {

    private Day8() {

    }

    public static void solve() throws IOException {
        List<String> lines = FileHelper.getLinesFromFile(FileHelper.getFileName(8));

        System.out.println("The value of the accumulator, immediately before any instruction is executed a second time, is: " + solvePart1(lines));
        System.out.println("The value of the accumulator at the end of the program is: " + solvePart2(lines));
    }

    /**
     * --- Day 8: Handheld Halting ---
     *
     * Your flight to the major airline hub reaches cruising altitude without incident.
     * While you consider checking the in-flight menu for one of those drinks that come with a little
     * umbrella, you are interrupted by the kid sitting next to you.
     *
     * Their handheld game console won't turn on! They ask if you can take a look.
     *
     * You narrow the problem down to a strange infinite loop in the boot code (your puzzle input) of
     * the device. You should be able to fix it, but first you need to be able to run the code in isolation.
     *
     * The boot code is represented as a text file with one instruction per line of text. Each instruction
     * consists of an operation (acc, jmp, or nop) and an argument (a signed number like +4 or -20).
     *
     *  - acc increases or decreases a single global value called the accumulator by the value given in the argument.
     *    For example, acc +7 would increase the accumulator by 7. The accumulator starts at 0. After an acc
     *    instruction, the instruction immediately below it is executed next.
     *
     *  - jmp jumps to a new instruction relative to itself. The next instruction to execute is found using the
     *    argument as an offset from the jmp instruction; for example, jmp +2 would skip the next instruction, jmp +1
     *    would continue to the instruction immediately below it, and jmp -20 would cause the instruction 20 lines
     *    above to be executed next.
     *
     *  - nop stands for No Operation - it does nothing. The instruction immediately below it is executed next.
     *
     * Run your copy of the boot code.
     * Immediately before any instruction is executed a second time, what value is in the accumulator?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static int solvePart1(List<String> lines) {
        return debugging(lines, createObjectsFromInput(lines), new ArrayList<>());
    }

    /**
     * --- Part Two ---
     *
     * After some careful analysis, you believe that exactly one instruction is corrupted.
     *
     * Somewhere in the program, either a jmp is supposed to be a nop, or a nop is supposed to be a jmp.
     * (No acc instructions were harmed in the corruption of this boot code.)
     *
     * The program is supposed to terminate by attempting to execute an instruction immediately after the
     * last instruction in the file. By changing exactly one jmp or nop, you can repair the boot code and
     * make it terminate correctly.
     *
     * Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp).
     * What is the value of the accumulator after the program terminates?
     *
     * @param lines the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021-04-25
     */
    private static int solvePart2(List<String> lines) {
        List<Operation> lOperations = createObjectsFromInput(lines);
        List<Operation> temp = copyOperationList(lOperations);

        int index = 0;
        while (index < lOperations.size()) {
            Operation operation = lOperations.get(index);
            changeOperations(temp, index, operation);

            List<Integer> executedIndices = new ArrayList<>();
            int accValue = 0;
            Operation tempOperation;
            for (int i = 0; i < temp.size(); i++) {
                if (executedIndices.contains(i)) {
                    break; // endless loop
                }
                executedIndices.add(i);

                tempOperation = temp.get(i);
                switch (tempOperation.getOperationType()) {
                    case ACCUMULATOR:
                        accValue = doMathOperation(tempOperation, accValue);
                        break;

                    case JUMP:
                        i = doMathOperation(tempOperation, i - 1);
                        break;

                    case NO_OPERATION:
                    default:
                        break;
                }

                if (i == temp.size() - 1) {
                    return accValue; // ende erreicht
                }
            }

            index++;
            temp = copyOperationList(lOperations);
        }

        return -1;
    }

    private static void changeOperations(List<Operation> temp, int index, Operation operation) {
        if (operation.getOperationType() == OperationType.JUMP) {
            temp.get(index).setOperationType(OperationType.NO_OPERATION);
        }
        else if (operation.getOperationType() == OperationType.NO_OPERATION) {
            temp.get(index).setOperationType(OperationType.JUMP);
        }
    }

    private static List<Operation> createObjectsFromInput(List<String> lines) {
        List<Operation> list = new ArrayList<>();

        Operation operation;
        for (String line : lines) {
            String[] splits = line.split(" ");

            operation = new Operation();
            operation.setOperationType(OperationType.getTypeByTag(splits[0]));
            operation.setValueSign(splits[1].charAt(0));
            operation.setValue(Integer.parseInt(splits[1].substring(1)));

            list.add(operation);
        }

        return list;
    }

    private static int debugging(List<String> lines, List<Operation> operationList, List<Integer> executedIndices) {
        int accValue = 0;
        Operation operation;
        for (int i = 0; i < operationList.size(); i++) {
            if (executedIndices.contains(i)) {
                break;
            }
            executedIndices.add(i);

            operation = operationList.get(i);
            switch (operation.getOperationType()) {
                case ACCUMULATOR:
                    accValue = doMathOperation(operation, accValue);
                    break;

                case JUMP:
                    i = doMathOperation(operation, i - 1);
                    break;

                case NO_OPERATION:
                default:
                    break;
            }
        }

        return accValue;
    }

    private static int doMathOperation(Operation operation, int value) {
        return operation.isValueNegative() ? value - operation.getValue() : value + operation.getValue();
    }

    private static List<Operation> copyOperationList(List<Operation> copyFrom) {
        List<Operation> copyTo = new ArrayList<>();
        copyFrom.forEach(operation -> copyTo.add(new Operation(operation)));

        return copyTo;
    }
}
