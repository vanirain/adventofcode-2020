package at.vaaniicx.aoc.day;

import at.vaaniicx.aoc.utils.FileHelper;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Day9 {

    private Day9() {

    }

    public static void solve() throws IOException {
        List<BigInteger> numbers = FileHelper.getLinesFromFile(FileHelper.getFileName(9)).stream().map(BigInteger::new).collect(Collectors.toList());

        System.out.println("The first number that does not meet the requirements: " + solvePart1(numbers));
        System.out.println("The encryption weakness in the XMAS-encrypted data: " + solvePart2(numbers));
    }

    /**
     * --- Day 9: Encoding Error ---
     *
     * With your neighbor happily enjoying their video game, you turn your attention to an open data port on the
     * little screen in the seat in front of you.
     *
     * Though the port is non-standard, you manage to connect it to your computer through the clever use of several
     * paperclips. Upon connection, the port outputs a series of numbers (your puzzle input).
     *
     * The data appears to be encrypted with the eXchange-Masking Addition System (XMAS) which, conveniently for you,
     * is an old cypher with an important weakness.
     *
     * XMAS starts by transmitting a preamble of 25 numbers. After that, each number you receive should be the sum of
     * any two of the 25 immediately previous numbers. The two numbers will have different values, and there might be
     * more than one such pair.
     *
     * The first step of attacking the weakness in the XMAS data is to find the first number in the list
     * (after the preamble) which is not the sum of two of the 25 numbers before it.
     *
     * What is the first number that does not have this property?
     *
     * @param numbers the puzzle input
     * @return      the puzzle answer
     * @author      vanirain
     * @since       2021
     */
    private static BigInteger solvePart1(List<BigInteger> numbers) {
        final int preambleRange = 25;
        List<BigInteger> preamble = retrievePreambleNumbers(numbers, preambleRange);

        for (int i = preambleRange; i < numbers.size(); i++) {
            BigInteger numberToCheck = numbers.get(i);

            if (isNumberMeetingRequirements(numbers, preamble, i, numberToCheck)) {
                updatePreamble(preamble, numbers.get(i));
                continue;
            }

            // es existiert keine Kombination für diese Zahl
            return numbers.get(i);
        }

        return null;
    }

    /**
     * --- Part Two ---
     *
     * The final step in breaking the XMAS encryption relies on the invalid number you just found: you must find a
     * contiguous set of at least two numbers in your list which sum to the invalid number from step 1.
     *
     * To find the encryption weakness, add together the smallest and largest number in this contiguous range;
     * in this example, these are 15 and 47, producing 62.
     *
     * What is the encryption weakness in your XMAS-encrypted list of numbers?
     *
     * @param numbers the puzzle input
     * @return        the puzzle answer
     * @author        vanirain
     * @since         2021
     */
    private static BigInteger solvePart2(List<BigInteger> numbers) {
        BigInteger invalidNumber = solvePart1(numbers);

        BigInteger sum = BigInteger.ZERO;
        for (int i = 0; i < numbers.size(); i++) { // start of sequence
            for (int k = i; k < numbers.size(); k++) { // changing "end" of sequence
                sum = sum.add(numbers.get(k));
                if (sum.equals(invalidNumber)) {
                    List<BigInteger> matchingSequence = numbers.subList(i, k);
                    BigInteger min = Collections.min(matchingSequence);
                    BigInteger max = Collections.max(matchingSequence);

                    return min.add(max);
                }
            }

            sum = BigInteger.ZERO;
        }

        return null;
    }

    private static boolean checkCombinations(List<BigInteger> preamble, BigInteger numberToCheck) {
        for (int j = 0; j < preamble.size(); j++) {
            for (int k = 0; k < preamble.size(); k++) {
                if (j == k) {
                    continue; // eine Zahl darf nicht mit sich selbst multipliziert werden
                }

                if (preamble.get(j).add(preamble.get(k)).equals(numberToCheck)) {
                    return true; // die Summe beider Zahlen ist equivalent zu der Zahl, die überprüft werden soll
                }
            }
        }

        return false;
    }

    private static List<BigInteger> retrievePreambleNumbers(List<BigInteger> numbers, int range) {
        List<BigInteger> preamble = new ArrayList<>();
        for (int i = 0; i < range; i++) {
            preamble.add(i, numbers.get(i));
        }
        return preamble;
    }

    private static boolean isNumberMeetingRequirements(List<BigInteger> numbers, List<BigInteger> preamble, int i, BigInteger numberToCheck) {
        return checkCombinations(preamble, numberToCheck);
    }

    private static void updatePreamble(List<BigInteger> preamble, BigInteger toAdd) {
        preamble.remove(0); // erstes Element der Präambel entfernen
        preamble.add(toAdd); // Element der Präambel hinzufügen
    }
}
