package at.vaaniicx.aoc.object;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bag {

    private String bagName;
    private Map<String, Integer> subBags; // Map<BagName, Amount>

    public Bag() {

    }

    public Bag(String bagName, Map<String, Integer> subBags) {
        if (subBags == null) {
            subBags = new HashMap<>();
        }

        this.bagName = bagName;
        this.subBags = subBags;
    }

    public boolean containsSubBag(String subBagName) {
        return this.subBags.containsKey(subBagName);
    }

    public boolean containsSubBag(List<Bag> bags) {
        for (Bag bag : bags) {
            if (this.subBags.containsKey(bag.getBagName())) {
                return true;
            }
        }

        return false;
    }

    public String getBagName() {
        return bagName;
    }

    public void setBagName(String bagName) {
        this.bagName = bagName;
    }

    public Map<String, Integer> getSubBags() {
        return subBags;
    }

    public void setSubBags(Map<String, Integer> subBags) {
        this.subBags = subBags;
    }
}
