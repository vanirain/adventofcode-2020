package at.vaaniicx.aoc.object;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Group {

    private Set<Character> uniqueAnswers;
    private List<String> answersPerMember;

    public Group() {

    }

    public Group(char[] answers) {
        this.uniqueAnswers = new HashSet<>();

        for (char answer : answers) {
            this.uniqueAnswers.add(answer);
        }
    }

    public Group(List<String> answersPerMember) {
        this.answersPerMember = new ArrayList<>(answersPerMember);
    }

    public Set<Character> getUniqueAnswers() {
        return uniqueAnswers;
    }

    public List<String> getAnswersPerMember() {
        return answersPerMember;
    }
}
