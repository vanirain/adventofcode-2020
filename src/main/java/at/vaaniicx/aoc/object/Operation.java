package at.vaaniicx.aoc.object;

import at.vaaniicx.aoc.object.type.OperationType;

public class Operation {

    private OperationType operationType;
    private char valueSign;
    private int value;

    public Operation() {

    }

    // Constructor by Copy
    public Operation(Operation operation) {
        this.operationType = operation.operationType;
        this.valueSign = operation.valueSign;
        this.value = operation.value;
    }

    public boolean isValueNegative() {
        return this.valueSign == '-';
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public void setValueSign(char valueSign) {
        this.valueSign = valueSign;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
