package at.vaaniicx.aoc.object;

import at.vaaniicx.aoc.utils.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Passport {
    private static final List<String> requiredFields;
    static {
        List<String> work = new ArrayList<>();
        work.add("byr");
        work.add("iyr");
        work.add("eyr");
        work.add("hgt");
        work.add("hcl");
        work.add("ecl");
        work.add("pid");
        requiredFields = Collections.unmodifiableList(work);
    }

    List<String> fields;
    List<String[]> data;

    public Passport() {

    }

    public Passport(List<String[]> data) {
        this.data = new ArrayList<>(data);
        this.fields = retrieveFieldsFromData();
    }

    public boolean hasRequiredFields() {
        return this.fields.containsAll(requiredFields);
    }

    private List<String> retrieveFieldsFromData() {
        if (this.data == null) return Collections.emptyList();

        List<String> lFields = new ArrayList<>();
        for (String[] d : data) {
           lFields.add(d[0]);
        }

        return lFields;
    }

    public boolean validateFields() {
        boolean isValid = true;

        for (String[] value : this.data) {
            switch (value[0]) {
                case "byr":
                    isValid = Validator.validateBYR(value[1]);
                    break;

                case "iyr":
                    isValid = Validator.validateIYR(value[1]);
                    break;

                case "eyr":
                    isValid = Validator.validateEYR(value[1]);
                    break;

                case "hgt":
                    isValid = Validator.validateHGT(value[1]);
                    break;

                case "hcl":
                    isValid = Validator.validateHCL(value[1]);
                    break;

                case "ecl":
                    isValid = Validator.validateECL(value[1]);
                    break;

                case "pid":
                    isValid = Validator.validatePID(value[1]);
                    break;
            }

            if (!isValid) {
                return false;
            }
        }

        return true;
    }

}