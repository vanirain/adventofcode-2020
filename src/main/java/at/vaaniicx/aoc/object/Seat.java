package at.vaaniicx.aoc.object;

public class Seat {

    private final String boardingPass;
    private int seatID;

    public Seat(String boardingPass) {
        this.boardingPass = boardingPass;
        calculateSeatID();
    }

    private void calculateSeatID() {
        String rowString = this.boardingPass.substring(0, 7);
        String columnString = this.boardingPass.substring(7);

        int rowLowest = 0;
        int rowHighest = 127;
        int diff;
        for (char rowLetter : rowString.toCharArray()) {
            diff = (int) Math.round((double) (rowHighest - rowLowest) / 2);

            if (rowLetter == 'F') {
                rowHighest -= diff;
            } else if (rowLetter == 'B') {
                rowLowest += diff;
            }
        }

        int columnLowest = 0;
        int columnHighest = 7;
        for (char columnLetter : columnString.toCharArray()) {
            diff = (int) Math.round((double) (columnHighest - columnLowest) / 2);

            if (columnLetter == 'L') {
                columnHighest -= diff;
            } else if (columnLetter == 'R') {
                columnLowest += diff;
            }
        }

        int row = rowHighest;
        int column = columnHighest;
        this.seatID = (row * 8) + column;
    }

    public int getSeatID() {
        return seatID;
    }
}
