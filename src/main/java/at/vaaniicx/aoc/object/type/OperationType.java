package at.vaaniicx.aoc.object.type;

import java.util.Arrays;

public enum OperationType {

    ACCUMULATOR("acc"),
    JUMP("jmp"),
    NO_OPERATION("nop");

    private final String tag;

    OperationType(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return this.tag;
    }

    public static OperationType getTypeByTag(String tag) {
        return Arrays.stream(OperationType.values())
                     .filter(e -> e.getTag().equalsIgnoreCase(tag))
                     .findFirst()
                     .orElse(null);
    }
}
