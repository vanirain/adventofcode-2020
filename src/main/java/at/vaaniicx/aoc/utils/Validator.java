package at.vaaniicx.aoc.utils;

import java.util.*;

public class Validator {

    private static final List<String> eyeColors;
    static {
        List<String> work = new ArrayList<>();
        work.add("amb");
        work.add("blu");
        work.add("brn");
        work.add("gry");
        work.add("grn");
        work.add("hzl");
        work.add("oth");
        eyeColors = Collections.unmodifiableList(work);
    }

    private Validator() {

    }

    public static boolean validateBYR(String data) {
        if (data.length() != 4) {
            return false;
        }

        int year;
        try {
            year = Integer.parseInt(data);
        } catch (NumberFormatException e) {
            return false;
        }

        return year >= 1920 && year <= 2002;
    }

    public static boolean validateIYR(String data) {
        if (data.length() != 4) {
            return false;
        }

        int year;
        try {
            year = Integer.parseInt(data);
        } catch (NumberFormatException e) {
            return false;
        }

        return year >= 2010 && year <= 2020;
    }

    public static boolean validateEYR(String data) {
        if (data.length() != 4) {
            return false;
        }

        int year;
        try {
            year = Integer.parseInt(data);
        } catch (NumberFormatException e) {
            return false;
        }

        return year >= 2020 && year <= 2030;
    }

    public static boolean validateHGT(String data) {
        String unit = data.substring(data.length() - 2);

        int height;
        try {
            height = Integer.parseInt(data.substring(0, data.length() - 2));
        } catch (NumberFormatException e) {
            return false;
        }

        switch (unit) {
            case "cm":
                if (height < 150 || height > 193) {
                    return false;
                }

                break;

            case "in":
                if (height < 59 || height > 76) {
                    return false;
                }

                break;

            default:
                return false;
        }

        return true;
    }

    public static boolean validateHCL(String data) {
        if (data.charAt(0) != '#') {
            return false;
        }

        String hex = data.substring(1);

        boolean isHex = hex.matches("[0-9a-f]+");
        return hex.length() == 6 && isHex;
    }

    public static boolean validateECL(String data) {
        return eyeColors.stream().anyMatch(s -> s.equalsIgnoreCase(data));
    }

    public static boolean validatePID(String data) {
        if (data.length() != 9) {
            return false;
        }

        return data.matches("[0-9]+");
    }
}